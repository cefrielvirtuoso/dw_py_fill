# -*- coding: utf-8 -*-

############################################################################### 
# @file     FillAnalyzer.py
# @author   Andrea Villa
# @author   Giuseppe Caccia
# @date     2018
# @brief    Script for the analsys of the dishwasher video captures.
###############################################################################
# @attention
#
# All information contained herein is, and remains the property
# of Cefriel. The intellectual and technical concepts contained
# herein are proprietary to Cefriel and may be covered by registered Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Cefriel.
# 
###############################################################################



import numpy as np
import cv2
import matplotlib.pyplot as plt
import Tkinter as tk
import tkFileDialog as filedialog
import glob

threshold = 13
blobArea = 135
fileIndex = 0
yGirante = 95
leftExcluded = 5
rightExcluded = 5
windowsName = "EXTRACTED LOAD"
windowsNameOrig = "Original Image"

print "CV2 version = ", cv2.__version__

def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()

def handleClosePlotWindow(evt):
    print('Closed Figure!')
    exit(0)

def changeBlobArea(area):
    global blobArea
    blobArea = area
    ComputeLoad()

def changeThreshold(th):
    global threshold
    threshold = th
    ComputeLoad()

def computeDiff(fileIndex):
    global imf
    global imd
    imf = cv2.imread(fileList[fileIndex], cv2.IMREAD_GRAYSCALE)
    imd = cv2.absdiff(imf, imb)
    #cv2.imshow(windowsNameOrig, imf)
    ComputeLoad()

def ComputeLoad():
    global blobArea
    global threshold

    print blobArea, threshold
    retvalT, imt = cv2.threshold(imd, threshold, 255, cv2.THRESH_BINARY_INV)

    kernel = np.ones((3,3),np.uint8)
    imtd = cv2.dilate(imt, kernel,iterations = 1)
    imte = cv2.erode(imtd, kernel,iterations = 1)

    for x in range(0, imte.shape[0]):
        imte[x,0] = 255
        imte[x,imte.shape[1]-1] = 255

    for y in range(0, imte.shape[1]):
        imte[0,y] = 255
        imte[imte.shape[0]-1,y] = 255

    im = imte

    # Setup SimpleBlobDetector parameters.
    params = cv2.SimpleBlobDetector_Params()

    # Default value of detection paarmeters:
    #SimpleBlobDetector::Params::Params()
    #{
    #    thresholdStep = 10;
    #    minThreshold = 50;
    #    maxThreshold = 220;
    #    minRepeatability = 2;
    #    minDistBetweenBlobs = 10;

    #    filterByColor = true;
    #    blobColor = 0;

    #    filterByArea = true;
    #    minArea = 25;
    #    maxArea = 5000;

    #    filterByCircularity = false;
    #    minCircularity = 0.8f;
    #    maxCircularity = std::numeric_limits<float>::max();

    #    filterByInertia = true;
    #    minInertiaRatio = 0.1f;
    #    maxInertiaRatio = std::numeric_limits<float>::max();

    #    filterByConvexity = true;
    #    minConvexity = 0.95f;
    #    maxConvexity = std::numeric_limits<float>::max();
    #}

    # Change thresholds
    params.minThreshold = 0
    params.maxThreshold = 256
     
    # Filter by Area.
    params.filterByArea = True
    params.minArea = blobArea
    params.maxArea = 320*150

    ## Filter by Circularity
    params.filterByCircularity = False
    params.minCircularity = 0.1

    ## Filter by Convexity
    params.filterByConvexity = False
    #params.minConvexity = 0.87
    
    ## Filter by Inertia
    params.filterByInertia = False
    #params.minInertiaRatio = 0.01

    # Create a detector with the parameters
    detector = cv2.SimpleBlobDetector_create(params)

    # Detect blobs.
    keypoints = detector.detect(im)

    keyPointsLimited = []
    for key in range(0, (len(keypoints))):
        if ((keypoints[key].pt[0] > leftExcluded) and ((keypoints[key].pt[0]) < (320 - rightExcluded))):
           keyPointsLimited.append(keypoints[key])
           area = 3.1416 * keypoints[key].size * keypoints[key].size / 4
           print 'keypoint:', key, '-', keypoints[key].pt[0], keypoints[key].pt[1], keypoints[key].size, 'Area:', area
        else:
            print 'keypoint---:', key, '-', keypoints[key].pt[0], keypoints[key].pt[1], keypoints[key].size, 'Area:', area


  
    keyPointsLimited_Up = []
    keyPointsLimited_Down = []
    for key in range(0, (len(keyPointsLimited))):
        if ((keyPointsLimited[key].pt[1] > yGirante)):
            keyPointsLimited_Down.append(keypoints[key])
            print 'keypoint DOWN:', key, '-', keyPointsLimited[key].size
        else:
            keyPointsLimited_Up.append(keypoints[key])
            print 'keypoint UP:', key, '-', keyPointsLimited[key].size

    # Draw detected blobs as red circles.
    # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures
    # the size of the circle corresponds to the size of blob
    im_with_keypoints_Up = cv2.drawKeypoints(im, keyPointsLimited_Up, np.array([]), (255,0,0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    im_with_keypoints = cv2.drawKeypoints(im_with_keypoints_Up, keyPointsLimited_Down, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    cv2.line(im_with_keypoints, (0, yGirante), (320, yGirante), (0,255,0), 2, 4)
    cv2.line(im_with_keypoints, (leftExcluded, 0), (leftExcluded,240), (0,255,0), 1)
    cv2.line(im_with_keypoints, (320-rightExcluded, 0), (320-rightExcluded,240), (0,255,0), 1)

    #for key in range(0, (len(keyPointsLimited))):
    #    cv2.line(im_with_keypoints,
    #             (int(keyPointsLimited[key].pt[0]-keyPointsLimited[key].size), int(keyPointsLimited[key].pt[1])),
    #             (int(keyPointsLimited[key].pt[0]+keyPointsLimited[key].size), int(keyPointsLimited[key].pt[1])),
    #             (0,0,255), 1)
    #    cv2.line(im_with_keypoints,
    #             (int(keyPointsLimited[key].pt[0]), int(keyPointsLimited[key].pt[1]-keyPointsLimited[key].size)),
    #             (int(keyPointsLimited[key].pt[0]), int(keyPointsLimited[key].pt[1]+keyPointsLimited[key].size)),
    #             (0,0,255), 1)

    CU = 0
    for key in range(0, (len(keyPointsLimited_Up))):
        keyPointArea = (3.1416 * (keyPointsLimited_Up[key].size) * (keyPointsLimited_Up[key].size) / 4)
        CU = CU + keyPointArea
        print 'Keypoint Up', key, 'Area', keyPointArea, 'Tot', CU
        cv2.line(im_with_keypoints,
                 (int(keyPointsLimited_Up[key].pt[0]-keyPointsLimited_Up[key].size), int(keyPointsLimited_Up[key].pt[1])),
                 (int(keyPointsLimited_Up[key].pt[0]+keyPointsLimited_Up[key].size), int(keyPointsLimited_Up[key].pt[1])),
                 (255,0,0), 1)
        cv2.line(im_with_keypoints,
                 (int(keyPointsLimited_Up[key].pt[0]), int(keyPointsLimited_Up[key].pt[1]-keyPointsLimited_Up[key].size)),
                 (int(keyPointsLimited_Up[key].pt[0]), int(keyPointsLimited_Up[key].pt[1]+keyPointsLimited_Up[key].size)),
                 (255,0,0), 1)

    CD = 0
    for key in range(0, (len(keyPointsLimited_Down))):
        keyPointArea = (3.1416 * (keyPointsLimited_Down[key].size) * (keyPointsLimited_Down[key].size) / 4)
        CD = CD + keyPointArea
        print 'Keypoint Down', key, 'Area', keyPointArea, 'Tot', CD
        cv2.line(im_with_keypoints,
                 (int(keyPointsLimited_Down[key].pt[0]-keyPointsLimited_Down[key].size), int(keyPointsLimited_Down[key].pt[1])),
                 (int(keyPointsLimited_Down[key].pt[0]+keyPointsLimited_Down[key].size), int(keyPointsLimited_Down[key].pt[1])),
                 (0,0,255), 1)
        cv2.line(im_with_keypoints,
                 (int(keyPointsLimited_Down[key].pt[0]), int(keyPointsLimited_Down[key].pt[1]-keyPointsLimited_Down[key].size)),
                 (int(keyPointsLimited_Down[key].pt[0]), int(keyPointsLimited_Down[key].pt[1]+keyPointsLimited_Down[key].size)),
                 (0,0,255), 1)

    # Show blobs
    #cv2.imshow(windowsName, im_with_keypoints)
    imf_color = cv2.cvtColor(imf, cv2.COLOR_GRAY2BGR)
    imf_color_with_keypoints_Up = cv2.drawKeypoints(imf_color, keyPointsLimited_Up, np.array([]), (255,0,0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    imf_color_with_keypoints = cv2.drawKeypoints(imf_color_with_keypoints_Up, keyPointsLimited_Down, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    rU = int(CU*100/320/yGirante)
    rD = int(CD*100/320/(239-yGirante))
    print 'AREA ', rU,'%',int(CU), '--', rD,'%',int(CD)
    cv2.rectangle(imf_color_with_keypoints,(0,0),(rU,5),(255,0,0),cv2.FILLED)
    cv2.rectangle(imf_color_with_keypoints,(0,yGirante+2),(rD*320/100,yGirante+7),(0,0,255),cv2.FILLED)
    img_concat = cv2.hconcat([im_with_keypoints, imf_color_with_keypoints])
    
    cv2.imshow(windowsName, img_concat)


# Clean the default tk window.
root = tk.Tk()
root.withdraw()

# Ask for the file to read.

dname = filedialog.askdirectory(title='Choose the input image folder')

fileList = []
#images = [cv2.imread(file) for file in glob.glob(dname + "/*.png")]
for file in glob.glob(dname + "/*.png"):
    fileList.append(file)
#fname = filedialog.askopenfilename(title='Choose the input image file')
fname = fileList[0]
# Read image
#imE = cv2.imread("blob.jpg", cv2.IMREAD_GRAYSCALE)

cv2.namedWindow(windowsName , cv2.WINDOW_AUTOSIZE )
cv2.createTrackbar("Th", windowsName, threshold, 255, changeThreshold)
cv2.createTrackbar("Blob A", windowsName, blobArea, 255, changeBlobArea)
cv2.createTrackbar("File", windowsName, fileIndex, len(fileList)-1, computeDiff)

#cv2.namedWindow(windowsNameOrig , cv2.WINDOW_AUTOSIZE )

imb = cv2.imread("empty.png", cv2.IMREAD_GRAYSCALE)

computeDiff(fileIndex)

ComputeLoad()

cv2.waitKey(0)
cv2.destroyAllWindows()